#!/bin/bash

NAME="mozio"
DJANGODIR=/home/ubuntu/projects/mozio/mozio
VIRTUAL_ENV=/home/ubuntu/.virtualenvs/mozio
USER=ubuntu
GROUP=ubuntu
NUM_WORKERS=2
DJANGO_SETTINGS_MODULE=mozio.settings.prod
DJANGO_WSGI_MODULE=mozio.wsgi
IP=0.0.0.0
PORT=8000

echo "Starting $NAME"

source $VIRTUAL_ENV/bin/activate
export PYTHONPATH=$VIRTUAL_ENV:$PYTHONPATH
echo $PYTHONPATH



exec $VIRTUAL_ENV/bin/gunicorn $DJANGO_WSGI_MODULE \
  --name $NAME \
  --bind=$IP:$PORT \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE \
  --chdir $DJANGODIR
  
#gunicorn mozio.wsgi -w 2 -b 0.0.0.0:8000 -e DJANGO_SETTINGS_MODULE=mozio.settings.prod --chdir /home/ubuntu/projects/mozio/mozio


#/home/ubuntu/.virtualenvs/mozio/bin/gunicorn mozio.wsgi --name "mozio" --bind=0.0.0.0:8000 --workers 2 --user=ubuntu --group=ubuntu --log-level=debug --env DJANGO_SETTINGS_MODULE=mozio.settings.prod --chdir /home/devel/projects/mozio/mozio