from .base import *

DEBUG = False

INSTALLED_APPS = INSTALLED_APPS + []

ALLOWED_HOSTS = ['104.131.83.56']

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'mozio',
        'USER': 'mozio',
        'PASSWORD': 'mozio',
        'HOST': 'localhost',
        'PORT': '',
    }
}
