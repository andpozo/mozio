from .base import *

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = INSTALLED_APPS + [
    'django_extensions',
    'debug_toolbar'
]

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'mozio',
        'USER': 'mozio',
        'PASSWORD': 'mozio',
        'HOST': 'localhost',
        'PORT': '',
    }
}
