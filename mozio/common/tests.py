import json

from django.contrib.auth.models import User
from django.contrib.gis.geos import GEOSGeometry
from django.core.urlresolvers import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from .models import Provider, ServiceArea


class ClientLogged(APITestCase):

    def setUp(self):
        self.user = User(username='test', is_superuser=True)
        self.user.set_password('test')
        self.user.save()
        self.client.login(username='test', password='test')


class ProviderTests(ClientLogged):

    def test_create_provider(self):
        """
        Ensure we can create a new provider object.
        """
        url = reverse('provider-list')
        data = {
            "name": "Andres Pozo",
            "email": "andpozo@gmail.com",
            "phone_number": "+5493512497857",
            "language": "es",
            "currency": "ARS"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Provider.objects.count(), 1)
        self.assertEqual(Provider.objects.get().name, data['name'])


class ServiceAreaTests(ClientLogged):

    def test_create_servicearea(self):
        """
        Ensure we can create a new servicearea object with GEOJSON format.
        """
        url = reverse('servicearea-list')
        data_provider = {
            "name": "Andres Pozo",
            "email": "andpozo@gmail.com",
            "phone_number": "+5493512497857",
            "language": "es",
            "currency": "ARS"
        }
        data = {
            "name": "Cordoba Capital",
            "provider": Provider.objects.create(**data_provider).id,
            "price": "100.00",
            "mpoly": {
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [
                                -64.30847167073696,
                                -31.30906179594282
                            ],
                            [
                                -64.05921935143533,
                                -31.30906179594282
                            ],
                            [
                                -64.05647276940445,
                                -31.524117414389952
                            ],
                            [
                                -64.3105316072599,
                                -31.522946786115803
                            ],
                            [
                                -64.30847167073696,
                                -31.30906179594282
                            ]
                        ]
                    ]
                ]
            }
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(ServiceArea.objects.count(), 1)
        self.assertEqual(ServiceArea.objects.get().name, data['name'])

    def test_ensure_point_is_contained(self):
        """
        Check API response:
            1) with a point inside MultiPolygon
            2) with a poing outside MultiPolygon
        """
        url = reverse('servicearea-list')
        data_provider = {
            "name": "Andres Pozo",
            "email": "andpozo@gmail.com",
            "phone_number": "+5493512497857",
            "language": "es",
            "currency": "ARS"
        }
        mp = {
            "type": "MultiPolygon",
            "coordinates": [
                [
                    [
                        [
                            -64.30847167073696,
                            -31.30906179594282
                        ],
                        [
                            -64.05921935143533,
                            -31.30906179594282
                        ],
                        [
                            -64.05647276940445,
                            -31.524117414389952
                        ],
                        [
                            -64.3105316072599,
                            -31.522946786115803
                        ],
                        [
                            -64.30847167073696,
                            -31.30906179594282
                        ]
                    ]
                ]
            ]
        }

        data_servicearea = {
            "name": "Cordoba Capital",
            "provider": Provider.objects.create(**data_provider),
            "price": "100.00",
            "mpoly": GEOSGeometry(json.dumps(mp))
        }
        point_outside = {
            "type": "Point",
            "coordinates": [-123.26436996459961, 44.564178042345375]
        }
        point_inside = {
            "type": "Point",
            "coordinates": [-64.187373, -31.3850624]
        }
        sa = ServiceArea.objects.create(**data_servicearea)

        response_out = self.client.get(
            url,
            {'mpoly__contains': json.dumps(point_outside)},
            format='json'
        )
        self.assertEqual(response_out.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_out.data['features']), 0)
        self.assertListEqual(response_out.data['features'], [])
        response_in = self.client.get(
            url,
            {'mpoly__contains': json.dumps(point_inside)},
            format='json'
        )
        self.assertEqual(response_in.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_in.data['features']), 1)
