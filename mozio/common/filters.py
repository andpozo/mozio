from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.db.models.fields import BaseSpatialField
from django.db.models import Q

from rest_framework_gis.filters import GeoFilterSet

from .models import ServiceArea


class ServiceAreaFilter(GeoFilterSet):

    class Meta:
        model = ServiceArea

    def filter_queryset(self, request, queryset, view):
        filter_fields = getattr(view, 'filter_fields', None)

        spatial_fields = [f.name for f in self.Meta.model._meta.fields
                          if f.name in filter_fields and
                          isinstance(f, BaseSpatialField)]

        params = request.query_params
        spatial_params = [p for p in params if p.split('__')[0] in spatial_fields]
        for spatial_param in spatial_params:
            try:
                lookup = GEOSGeometry(params.get(spatial_param))
                queryset = queryset.filter(Q(**{spatial_param: lookup}))
            except (ValueError, TypeError):
                pass
        return queryset
