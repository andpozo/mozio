from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.conf import settings


class Provider(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    phone_number = models.CharField(max_length=20)
    language = models.CharField(max_length=10,
                                choices=settings.LANGUAGES)
    currency = models.CharField(max_length=10,
                                choices=settings.CURRENCY_CHOICES)

    def __str__(self):
        return self.name


class ServiceArea(models.Model):
    name = models.CharField(max_length=50)
    provider = models.ForeignKey('common.Provider')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    mpoly = models.MultiPolygonField()

    def __str__(self):
        return self.name
