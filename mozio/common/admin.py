from django.contrib.gis import admin
from django.contrib import admin as default_admin
from .models import Provider, ServiceArea

admin.site.register(ServiceArea, admin.OSMGeoAdmin)
default_admin.site.register(Provider)
