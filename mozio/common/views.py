from rest_framework import viewsets, filters

from .filters import ServiceAreaFilter
from .models import Provider, ServiceArea
from .serializers import ProviderSerializer, ServiceAreaSerializer


class ProviderViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited Provider Model.
    """
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('name', 'email')


class ServiceAreaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited ServiceAreaSerializer Model.
    """
    queryset = ServiceArea.objects.all()
    serializer_class = ServiceAreaSerializer
    filter_backends = (filters.DjangoFilterBackend, ServiceAreaFilter)
    filter_fields = ('mpoly', 'price')
